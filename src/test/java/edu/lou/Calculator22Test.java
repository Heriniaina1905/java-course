package edu.lou;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class Calculator22Test {

	private Calculator22 subject;

	@Before
	public void setUp() {
		subject = new Calculator22();
	}

	@Test
	public void addSquare() {
		// given
		Integer three = 3;
		Integer two = 2;

		// when
		Integer res = subject.doBinaryOp(three, two, (Integer n1, Integer n2) -> n1 * n1 + n2 * n2);

		// then
		assertEquals(13, (int) res);
	}
}
