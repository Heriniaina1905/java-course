package edu.lou;

import static org.apache.commons.math3.primes.Primes.isPrime;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.ForkJoinPool;
import java.util.function.BiFunction;
import java.util.stream.IntStream;

public class PrimeCounter implements BiFunction<Integer, Integer, Integer> {

    private final ExecutorService es;

    public PrimeCounter() {
        es = ForkJoinPool.commonPool();
    }

    public PrimeCounter(ExecutorService es) {
        this.es = es;
    }

    @Override
    public Integer apply(Integer from, Integer to) {
        assert from >= 0 && from <= to;
        try {
            return es.submit(() -> (int) IntStream.rangeClosed(from, to)
                    .parallel()
                    .filter(n -> isPrime(n))
                    .count()).get();
        } catch (InterruptedException | ExecutionException e) {
            throw new RuntimeException(e);
        }
    }
}
