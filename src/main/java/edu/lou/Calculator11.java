package edu.lou;

public class Calculator11 {

	public Integer addSquare(Integer a, Integer b) {
		return a * a + b * b;
	}

	public Integer subSquare(Integer a, Integer b) {
		return a * a - b * b;
	}
}