package edu.lou;

import static java.lang.Integer.parseInt;
import static java.lang.System.out;

import org.apache.commons.math3.fraction.Fraction;

public class FractionAdder {

	public static void main(String[] args) {
		out.println(toFraction(args[0]).add(toFraction(args[1])));
	}

	private static Fraction toFraction(String fractionAsString) {
		String[] asStringArray = fractionAsString.split("/");
		return new Fraction(parseInt(asStringArray[0]), parseInt(asStringArray[1]));
	}
}
